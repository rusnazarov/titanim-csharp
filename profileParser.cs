﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Newtonsoft.Json.Linq;

namespace TitanIM.vk
{
    public class ProfileParser
    {
        public static ProfileItem parser(JToken json)
        {
            ProfileItem profile = new ProfileItem();

            try
            {
                profile.uid = (int)json["uid"];
                profile.firstName = (string)json["first_name"];
                profile.lastName = (string)json["last_name"];
                profile.sex = json["sex"] != null ? (Sex)((int)json["sex"]) : Sex.unknown;
                profile.photoMediumRec = json["photo_medium_rec"] != null ? Utils.photoUrlFix((string)json["photo_medium_rec"])
                                                                          : Utils.photoUrlFix((string)json["photo_medium"]);
                profile.lastSeen = json["last_seen"] != null ? (int)json["last_seen"]["time"] : 0;
                profile.online = (int)json["online"] == 1 ? true : false;
            }
            catch
            {
            }

            return profile;
        }

        public static ProfileItem parser(string response)
        {
            JObject json = JObject.Parse(response);
            JArray responseArray = (JArray)json["response"];
            return parser(responseArray[0]);
        }
    }
}
