﻿using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace TitanIM.vk
{
    public class MessageList : ObservableCollection<MessageItem>
    {
        public MessageList()
            : base()
        {
        }

        public int IndexOf(int mid)
        {
            for (int i = 0; i < this.Count; i++)
            {
                if (this[i].mid == mid)
                {
                    return i;
                }
            }

            return -1;
        }

        public int IndexOf(int uid, int mid)
        {
            for (int i = 0; i < this.Count; i++)
            {
                if (this[i].uid == uid && this[i].mid == mid)
                {
                    return i;
                }
            }

            return -1;
        }

        public int ChatIndexOf(int uid)
        {
            for (int i = 0; i < this.Count; i++)
            {
                if (this[i].uid == uid && !this[i].isGroupChat)
                {
                    return i;
                }
            }

            return -1;
        }

        public int GroupChatIndexOf(int chatId)
        {
            for (int i = 0; i < this.Count; i++)
            {
                if (this[i].chatId == chatId && this[i].isGroupChat)
                {
                    return i;
                }
            }

            return -1;
        }

        public MessageItem GetMessage(int mid)
        {
            return this[IndexOf(mid)];
        }

        public void ReplaceMessage(int index, MessageItem message)
        {
            this[index] = message;
        }

        public void Add(MessageList messages)
        {
            for (int i = 0; i < messages.Count; i++)
            {
                Add(messages[i]);
            }
        }

        public void Insert(int index, List<MessageItem> messages)
        {
            for (int i = 0; i < messages.Count; i++)
            {
                Insert(index, messages[i]);
            }
        }
    }
}
