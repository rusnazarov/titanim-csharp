﻿using System;
using System.Net;
using System.Windows;
using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO;
using Newtonsoft.Json.Linq;

namespace TitanIM.vk
{
    public struct AuthVars
    {
        public string phone;
        public string first_name;
        public string last_name;
        public string client_id;
        public string client_secret;
        public string password;
        public bool voice;
        public string sid;
        public string code;
    }

    public class Signup
    {
        string apiServer;
        AuthVars authVars;
        ErrorResponse errorPhone;

        public event EventHandler signupFinished;
        public event EventHandler confirmFinished;
        public event EventHandler<ErrorEventArgs> error;

        public Signup(string client_id, string client_secret)
        {
            apiServer = "https://api.vk.com";
            authVars.client_id = client_id;
            authVars.client_secret = client_secret;
            errorPhone = null;
            authVars.voice = false;
        }

        public string phone
        {
            get
            {
                return authVars.phone;
            }
            set
            {
                if (authVars.phone != value)
                {
                    authVars.phone = value;
                    OnPhoneChanged(authVars.phone);
                }
            }
        }

        public string firstName
        {
            get
            {
                return authVars.first_name;
            }
            set
            {
                if (authVars.first_name != value)
                {
                    authVars.first_name = value;
                    OnFirstNameChanged(authVars.first_name);
                }
            }
        }

        public string lastName
        {
            get
            {
                return authVars.last_name;
            }
            set
            {
                if (authVars.last_name != value)
                {
                    authVars.last_name = value;
                    OnLastNameChanged(authVars.last_name);
                }
            }
        }

        public string sid
        {
            get { return authVars.sid; }
            set { authVars.sid = value; }
        }

        public string password
        {
            get
            {
                return authVars.password;
            }
            set
            {
                if (authVars.password != value)
                {
                    authVars.password = value;
                    OnPasswordChanged(authVars.password);
                }
            }
        }

        public string code 
        {
            get { return authVars.code; } 
            set { authVars.code = value; }
        }

        public bool voice
        {
            get { return authVars.voice; }
            set { authVars.voice = value; }
        }

        private void checkPhone(string phone)
        {
            string query = string.Format("/method/auth.checkPhone?phone={0}&client_id={1}&client_secret={2}",
                                            phone,
                                            authVars.client_id,
                                            authVars.client_secret);

            HttpWebRequest http = (HttpWebRequest)WebRequest.Create(apiServer + query);
            http.ContentType = "application/x-www-form-urlencoded";
            http.Method = "POST";
            http.BeginGetResponse(new AsyncCallback(checkPhoneFinished), http);
        }

        private void checkPhoneFinished(IAsyncResult e)
        {
            //получили ответ от сервера
            string response = null;
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)e.AsyncState;
                HttpWebResponse webResponse = (HttpWebResponse)webRequest.EndGetResponse(e);
                StreamReader responseReader = new StreamReader(webResponse.GetResponseStream());
                response = responseReader.ReadToEnd();
            }
            catch (WebException webException)
            {
                //todo
                return;
            }

            Debug.WriteLine(response);

            JObject json = JObject.Parse(response);
            try
            {
                string result = json["response"].ToString();
                errorPhone = null;
                return;
            }
            catch
            {
                errorPhone = new ErrorResponse(json);
                return;
            }
        }

        public void authSignup()
        {
            if (errorPhone != null && errorPhone.code != Error.tryLater)
            {
                OnError(errorPhone);
                return;
            }

            string query = string.Format("/method/auth.signup?phone={0}&first_name={1}&last_name={2}&client_id={3}&client_secret={4}&voice={5}",
                                            phone,
                                            firstName,
                                            lastName,
                                            authVars.client_id,
                                            authVars.client_secret,
                                            voice ? "1" : "0");

            if (!string.IsNullOrEmpty(sid))
            {
                query += string.Format("&sid={0}", sid);
            }

            voice = false;

            HttpWebRequest http = (HttpWebRequest)WebRequest.Create(apiServer + query);
            http.ContentType = "application/x-www-form-urlencoded";
            http.Method = "POST";
            http.BeginGetResponse(new AsyncCallback(authSignupFinished), http);
        }

        private void authSignupFinished(IAsyncResult e)
        {
            //получили ответ от сервера
            string response = null;
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)e.AsyncState;
                HttpWebResponse webResponse = (HttpWebResponse)webRequest.EndGetResponse(e);
                StreamReader responseReader = new StreamReader(webResponse.GetResponseStream());
                response = responseReader.ReadToEnd();
            }
            catch (WebException webException)
            {
                //todo
                return;
            }

            Debug.WriteLine(response);//todo убрать

            JObject json = JObject.Parse(response);
            try
            {
                JToken result = (JToken)json["response"];
                sid = result["sid"].ToString();
                OnSignupFinished();
                return;
            }
            catch
            {
                ErrorResponse errorResponse = new ErrorResponse(json);
                OnError(errorResponse);
                return;
            }
        }

        public void authConfirm()
        {
            if (string.IsNullOrEmpty(code) || string.IsNullOrEmpty(password))
            {
                return;
            }

            string query = string.Format("/method/auth.confirm?phone={0}&code={1}&password={2}&client_id={3}&client_secret={4}",
                                            phone,
                                            code,
                                            password,
                                            authVars.client_id,
                                            authVars.client_secret);

            HttpWebRequest http = (HttpWebRequest)WebRequest.Create(apiServer + query);
            http.ContentType = "application/x-www-form-urlencoded";
            http.Method = "POST";
            http.BeginGetResponse(new AsyncCallback(authConfirmFinished), http);
        }

        private void authConfirmFinished(IAsyncResult e)
        {
            //получили ответ от сервера
            string response = null;
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)e.AsyncState;
                HttpWebResponse webResponse = (HttpWebResponse)webRequest.EndGetResponse(e);
                StreamReader responseReader = new StreamReader(webResponse.GetResponseStream());
                response = responseReader.ReadToEnd();
            }
            catch (WebException webException)
            {
                //todo
                return;
            }

            Debug.WriteLine(response);//todo убрать

            JObject json = JObject.Parse(response);
            try
            {
                JToken result = (JToken)json["response"];
                string uid = result["uid"].ToString();
                OnConfirmFinished();
                return;
            }
            catch
            {
                ErrorResponse errorResponse = new ErrorResponse(json);
                OnError(errorResponse);
                return;
            }
        }

        protected void OnPhoneChanged(string phone)
        {
            errorPhone = null;
            sid = "";
            if (phone.Length > 0 && (string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName)))
            {
                checkPhone(phone);
            }
        }

        protected void OnFirstNameChanged(string firstName)
        {
            //проверяем правильность имени, спецсимволы итд
        }

        protected void OnLastNameChanged(string lastName)
        {
            //проверяем правильность фамилии, спецсимволы итд
        }

        protected void OnPasswordChanged(string lastName)
        {
            //проверяем пароль, как минимум условие length > 6
        }

        protected void OnSignupFinished()
        {
            if (signupFinished != null)
            {
                signupFinished(this, EventArgs.Empty);
            }
        }

        protected void OnConfirmFinished()
        {
            if (confirmFinished != null)
            {
                confirmFinished(this, EventArgs.Empty);
            }
        }

        protected void OnError(ErrorResponse errorResponse)
        {
            if (error != null)
            {
                ErrorEventArgs arg = new ErrorEventArgs();
                arg.error = errorResponse.code;
                arg.text = errorResponse.msg;
                arg.fatal = errorResponse.fatal;
                error(this, arg);
            }
        }
    }
}
