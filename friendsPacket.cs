﻿using System;
using System.Diagnostics; //todo убрать
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Newtonsoft.Json.Linq;

namespace TitanIM.vk
{
    public class FriendsPacket
    {
        Connection connection;

        public event EventHandler<FriendListEventArgs> friends;

        public string fields { get; set; }

        public FriendsPacket(Connection connection)
        {
            this.connection = connection;
        }

        public void load()
        {
            Packet packet = new Packet("execute");
            string sсript = "var favorites=API.friends.get({\"fields\":\"" + fields + "\",\"count\":5,\"order\":\"hints\"});"
                          + "var friends=API.friends.get({\"fields\":\"" + fields + "\",\"order\":\"name\"});"
                          + "return {\"favorites\":favorites, \"friends\":friends};";
            packet.addParam("code", sсript);

            packet.finished += OnPacketFinished;
            connection.appendQuery(packet);
        }

        void OnPacketFinished(object sender, PacketEventArgs e)
        {
            try
            {
                JObject json = JObject.Parse(e.result);
                JToken response = json["response"];

                FriendList friendList = new FriendList();

                //извлекаю важные контакты
                foreach (var item in (JArray)response["favorites"])
                {
                    ProfileItem profile = ProfileParser.parser(item);
                    profile.alphabet = "\u2605";
                    friendList.Add(profile);
                }

                //извлекаю контакты //todo убрать копипаст
                foreach (var item in (JArray)response["friends"])
                {
                    ProfileItem profile = ProfileParser.parser(item);
                    profile.alphabet = char.ToLower(profile.firstName[0]).ToString();
                    friendList.Add(profile);
                }

                //событие
                if (friends != null)
                {
                    FriendListEventArgs arg = new FriendListEventArgs();
                    arg.friendList = friendList;
                    friends(this, arg);
                }
            }
            catch
            {
            }
        }
    }
}
