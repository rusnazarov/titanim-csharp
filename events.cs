﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace TitanIM.vk
{
    public class ConnectedEventArgs : EventArgs
    {
        public string token;
        public string secret;
        public int uid;
    }

    public class ErrorEventArgs : EventArgs
    {
        public Error error;
        public string text;
        public bool global;
        public bool fatal;
    }

    public class CaptchaEventArgs : EventArgs
    {
        public string captcha_sid;
        public string captcha_img;
    }

    public class PacketEventArgs : EventArgs
    {
        public string result;
    }

    public class MessageDeletedEventArgs : EventArgs
    {
        public int mid;
    }

    public class MessageFlagsReplacedEventArgs : EventArgs
    {
        public int mid;
        public int flags;
    }

    public class MessageFlagsSetEventArgs : EventArgs
    {
        public int mid;
        public int mask;
        public int uid;
    }

    public class MessageFlagsResetedEventArgs : EventArgs
    {
        public int mid;
        public int mask;
        public int uid;
    }

    public class MessageEventArgs : EventArgs
    {
        public MessageItem message;
    }

    public class UserStatusChangedEventArgs : EventArgs
    {
        public int uid;
        public bool online;
    }

    public class GroupChatUpdatedEventArgs : EventArgs
    {
        public int chat_id;
        public bool self;
    }

    public class ChatTypingEventArgs : EventArgs
    {
        public int uid;
        public int flags;
    }

    public class GroupChatTypingEventArgs : EventArgs
    {
        public int uid;
        public int chat_id;
    }

    public class UserCallEventArgs : EventArgs
    {
        public int uid;
        public string call_id;
    }

    public class FriendListEventArgs : EventArgs
    {
        public FriendList friendList;
    }

    public class MessageListEventArgs : EventArgs
    {
        public MessageList messageList;
    }

    public class CountersEventArgs : EventArgs
    {
        public int newMessages;
        public int requestsFriends;
    }

    public class MessageSentEventArgs : EventArgs
    {
        public int oldMid;
        public int newMid;
    }

    public class UploadResultEventArgs : EventArgs
    {
        public string result;
    }

    public class ProfileEventArgs : EventArgs
    {
        public ProfileItem profile;
    }
}
