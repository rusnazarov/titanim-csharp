﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Newtonsoft.Json.Linq;
using System.IO.IsolatedStorage;

namespace TitanIM.vk
{
    public class UploadAttachments
    {
        Dispatcher Dispatcher = Application.Current.RootVisual.Dispatcher;
        Connection connection;
        AttachmentList attachments;
        int index;
        int countUploading;

        public event EventHandler finished;

        public UploadAttachments(Connection connection)
        {
            this.connection = connection;
        }

        public void setAttachments(AttachmentList attachments)
        {
            this.attachments = attachments;
            index = 0;
            countUploading = attachments.Count;
        }

        public void upload()
        {
            if (index >= attachments.Count)
            {
                OnFinished();
                return;
            }

            switch (attachments[index].attachmentType)
            {
                case AttachmentType.PhotoFile:
                    {
                        PhotoFileItem photoFile = attachments[index] as PhotoFileItem;
                        if (!photoFile.isUploading)
                        {
                            photoFile.uploadProgress = 10;
                            Packet getUploadServer = new Packet("photos.getMessagesUploadServer");
                            getUploadServer.id = index;
                            getUploadServer.finished += OnGetUploadServerFinished;
                            connection.appendQuery(getUploadServer);
                        }
                        break;
                    }

                default:
                    {
                        countUploading--;
                        index++;
                        upload();
                        break;
                    }
            }
        }

        void OnGetUploadServerFinished(object sender, PacketEventArgs e)
        {
            try
            {
                Packet packet = sender as Packet;
                int idx = packet.id;

                JObject json = JObject.Parse(e.result);
                string uploadUrl = (string)(json["response"]["upload_url"]);

                UploadFile uploadFile = new UploadFile();
                uploadFile.id = idx;
                uploadFile.finished += OnUploadFileFinished;
                uploadFile.upload(uploadUrl, (attachments[idx] as PhotoFileItem).fileName, "photo", "image/" + System.IO.Path.GetExtension((attachments[idx] as PhotoFileItem).fileName).ToLower());

                Dispatcher.BeginInvoke(() =>
                {
                    (attachments[idx] as PhotoFileItem).uploadProgress += 10;
                    index++;
                    upload();
                });
            }
            catch
            {
            }
        }

        void OnUploadFileFinished(object sender, UploadResultEventArgs e)
        {
            try
            {
                UploadFile packet = sender as UploadFile;
                int idx = packet.id;

                JObject json = JObject.Parse(e.result);
                Packet saveMessagesPhoto = new Packet("photos.saveMessagesPhoto");
                saveMessagesPhoto.addParam("server", json["server"].ToString());
                saveMessagesPhoto.addParam("photo", json["photo"].ToString());
                saveMessagesPhoto.addParam("hash", json["hash"].ToString());
                saveMessagesPhoto.id = idx;
                saveMessagesPhoto.finished += OnSaveMessagesPhotoFinished;

                Dispatcher.BeginInvoke(() =>
                {
                    (attachments[idx] as PhotoFileItem).uploadProgress += 70;
                    connection.appendQuery(saveMessagesPhoto);
                });
            }
            catch
            {
            }
        }

        void OnSaveMessagesPhotoFinished(object sender, PacketEventArgs e)
        {
            try
            {
                Packet packet = sender as Packet;
                int idx = packet.id;

                JObject json = JObject.Parse(e.result);
                Dispatcher.BeginInvoke(() =>
                {
                    (attachments[idx] as PhotoFileItem).id = (string)(json["response"][0]["id"]);
                    (attachments[idx] as PhotoFileItem).srcBig = (string)(json["response"][0]["src_big"]);
                    (attachments[idx] as PhotoFileItem).uploadProgress = 100;
                    IsolatedStorageFile.GetUserStoreForApplication().DeleteFile((attachments[idx] as PhotoFileItem).fileName);

                    countUploading--;
                    OnFinished();
                });
            }
            catch
            {
            }
        }

        protected void OnFinished()
        {
            if (countUploading == 0 && finished != null)
            {
                finished(this, EventArgs.Empty);
            }
        }
    }
}
