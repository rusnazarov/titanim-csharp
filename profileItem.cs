﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace TitanIM.vk
{
    public class ProfileItem : INotifyPropertyChanged
    {
        int _uid;
        string _firstName;
        string _lastName;
        Sex _sex;
        string _photoMediumRec;
        bool _online;
        int _lastSeen;
        bool _isChecked;

        public event PropertyChangedEventHandler PropertyChanged;

        public int uid
        {
            get { return _uid; }
            set 
            {
                _uid = value;
                OnPropertyChanged("uid");
            }
        }

        public string firstName
        {
            get { return _firstName; }
            set 
            {
                _firstName = value;
                OnPropertyChanged("firstName");
                OnPropertyChanged("fullName");
            }
        }

        public string lastName
        {
            get { return _lastName; }
            set 
            {
                _lastName = value;
                OnPropertyChanged("lastName");
                OnPropertyChanged("fullName");
            }
        }

        public string fullName
        {
            get { return firstName + " " + lastName; }
        }

        public Sex sex
        {
            get { return _sex; }
            set
            {
                _sex = value;
                OnPropertyChanged("sex");
            }
        }

        public string photoMediumRec
        {
            get { return _photoMediumRec; }
            set
            {
                _photoMediumRec = value;
                OnPropertyChanged("photoMediumRec");
            }
        }

        public bool online
        {
            get { return _online; }
            set
            {
                _online = value;
                OnPropertyChanged("online");
            }
        }

        public int lastSeen
        {
            get { return _lastSeen; }
            set
            {
                _lastSeen = value;
                OnPropertyChanged("lastSeen");
            }
        }

        public bool isChecked
        {
            get { return _isChecked; }
            set
            {
                _isChecked = value;
                OnPropertyChanged("isChecked");
            }
        }

        public string alphabet
        {
            get;
            set;
        }

        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
