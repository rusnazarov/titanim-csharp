﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Newtonsoft.Json.Linq;

namespace TitanIM.vk
{
    public class VideoParser
    {
        public static VideoItem parser(JToken json)
        {
            VideoItem video = new VideoItem();
            string[] resolution = new string[] { "mp4_480", "mp4_360", "mp4_240", "mp4_720", "external", "flv_320", "flv_240" };

            try
            {
                video.vid = (int)json["vid"];
                video.ownerId = (int)json["owner_id"];
                video.title = (string)json["title"];
                video.duration = (int)json["duration"];
                video.description = (string)json["description"];
                video.imageBig = json["image_big"] != null ? (string)json["image_big"] : (string)json["image"];
                video.url = null;

                if (json["files"] != null)
                {
                    for (int i = 0; i < resolution.Length; i++)
                    {
                        if (json["files"][resolution[i]] != null)
                        {
                            video.url = (string)json["files"][resolution[i]];
                            break;
                        }
                    }
                }
            }
            catch
            {
            }

            return video;
        }
    }
}
