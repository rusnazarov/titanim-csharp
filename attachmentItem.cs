﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace TitanIM.vk
{
    public enum AttachmentType
    {
        Photo,
        Video,
        Audio,
        Doc,
        Map,
        FwdMessages,
        PhotoFile
    }

    public class AttachmentItem
    {
        public AttachmentType attachmentType
        {
            get;
            protected set;
        }
    }
}
