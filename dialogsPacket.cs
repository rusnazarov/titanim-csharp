﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Newtonsoft.Json.Linq;

namespace TitanIM.vk
{
    public class DialogsPacket
    {
        Connection connection;

        public event EventHandler<MessageListEventArgs> dialogs;
        public event EventHandler<CountersEventArgs> counters;

        public DialogsPacket(Connection connection)
        {
            this.connection = connection;
        }

        public void load(int offset = 0, int count = 20)
        {
            Packet packet = new Packet("execute");
            string sсript;

            if (offset == 0)
            {
                sсript = "var d=API.messages.getDialogs({\"offset\":" + offset.ToString() + ",\"count\":" + count.ToString() + ",\"preview_length\":50});"
                       + "var p=API.getProfiles({\"uids\":d@.uid+d@.chat_active,\"fields\":\"photo_medium_rec,online,last_seen,sex\"});"
                       + "var m=API.messages.get({\"filters\":1,\"preview_length\":1});"
                       + "var f=API.friends.getRequests({\"count\":1000});"
                       + "return {\"dialogs\":d,\"profiles\":p,\"countNewMsg\":m.length-1,\"countNewFriends\":f.length};";
            }
            else
            {
                sсript = "var d=API.messages.getDialogs({\"offset\":" + offset.ToString() + ",\"count\":" + count.ToString() + ",\"preview_length\":50});"
                       + "var p=API.getProfiles({\"uids\":d@.uid+d@.chat_active,\"fields\":\"photo_medium_rec,online,last_seen,sex\"});"
                       + "return {\"dialogs\":d,\"profiles\":p};";
            }

            packet.addParam("code", sсript);
            packet.finished += OnPacketFinished;
            connection.appendQuery(packet);
        }

        void OnPacketFinished(object sender, PacketEventArgs e)
        {
            try
            {
                JObject json = JObject.Parse(e.result);
                JToken response = json["response"];

                MessageList messageList = new MessageList();
                Dictionary<int, ProfileItem> profiles = new Dictionary<int, ProfileItem>(); 

                foreach (var item in (JArray)response["profiles"])
                {
                    ProfileItem profile = ProfileParser.parser(item);
                    profiles[profile.uid] = profile;
                }

                JArray dialogsArray = (JArray)response["dialogs"];
                for (int i = 1; i < dialogsArray.Count; i++)
                {
                    JToken dialog = dialogsArray[i];
                    MessageItem message = MessageParser.parser(dialog);

                    if (string.IsNullOrEmpty(message.body) && message.attachments != null && message.attachments.Count > 0)
                    {
                        message.body = Utils.attachmentTypeToString(message.attachments[0].attachmentType);
                    }
                    else
                    {
                        message.body = message.body.Replace("\n", " ");
                    }
                    
                    message.firstName = profiles[message.uid].firstName;
                    message.lastName = profiles[message.uid].lastName;
                    message.sex = profiles[message.uid].sex;

                    if (message.isGroupChat)
                    {
                        message.displayName = message.title;

                        try
                        {
                            string[] chatUsers = message.chatActive.Split(',');
                            message.photoMediumRec = profiles[int.Parse(chatUsers[0])].photoMediumRec;
                            message.photoMediumRec2 = profiles[int.Parse(chatUsers[1])].photoMediumRec;
                            message.photoMediumRec3 = profiles[int.Parse(chatUsers[2])].photoMediumRec;
                            message.photoMediumRec4 = profiles[int.Parse(chatUsers[3])].photoMediumRec;
                        }
                        catch
                        {
                        }

                        message.online = false;
                    }
                    else
                    {
                        message.displayName = profiles[message.uid].fullName;
                        message.photoMediumRec = profiles[message.uid].photoMediumRec;
                        message.online = profiles[message.uid].online;
                    }
                    
                    message.lastSeen = profiles[message.uid].lastSeen;
                    messageList.Add(message);
                }

                if (dialogs != null)
                {
                    MessageListEventArgs arg = new MessageListEventArgs();
                    arg.messageList = messageList;
                    dialogs(this, arg);
                }

                if (counters != null && response["countNewMsg"] != null)
                {
                    CountersEventArgs arg = new CountersEventArgs();
                    arg.newMessages = (int)response["countNewMsg"];
                    arg.requestsFriends = (int)response["countNewFriends"];
                    counters(this, arg);
                }
            }
            catch
            {
                if (dialogs != null)
                {
                    MessageListEventArgs arg = new MessageListEventArgs();
                    arg.messageList = new MessageList();
                    dialogs(this, arg);
                }
            }
        }
    }
}
