﻿using System;
using System.Collections.Generic;
using System.Diagnostics; //todo убрать
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO;
using Newtonsoft.Json.Linq;

namespace TitanIM.vk
{
    public enum UpdateType
    {
        MessageDeleted = 0,
        MessageFlagsReplaced = 1,
        MessageFlagsSet = 2,
        MessageFlagsReseted = 3,
        MessageAdded = 101,
        UserOnline = 8,
        UserOffline = 9,
        GroupChatUpdated = 51,
        ChatTyping = 61,
        GroupChatTyping = 62,
        UserCall = 70
    }

    public enum MessageFlag
    {
        Unread = 1,
        Outbox = 2,
        Replied = 4,
        Important = 8,
        Chat = 16,
        Friends = 32,
        Spam = 64,
        Deleted = 128,
        Fixed = 256,
        Media = 512
    }

    public class LongPoll
    {
        LongPollVars longPollVars;
        Connection connection;
        bool running;

        public event EventHandler<MessageDeletedEventArgs> messageDeleted;
        public event EventHandler<MessageFlagsReplacedEventArgs> messageFlagsReplaced;
        public event EventHandler<MessageFlagsSetEventArgs> messageFlagsSet;
        public event EventHandler<MessageFlagsResetedEventArgs> messageFlagsReseted;
        public event EventHandler<MessageEventArgs> messageAdded;
        public event EventHandler<UserStatusChangedEventArgs> userStatusChanged;
        public event EventHandler<GroupChatUpdatedEventArgs> groupChatUpdated;
        public event EventHandler<ChatTypingEventArgs> chatTyping;
        public event EventHandler<GroupChatTypingEventArgs> groupChatTyping;
        public event EventHandler<UserCallEventArgs> userCall;

        public LongPoll(Connection connection)
        {
            this.connection = connection;

            //по умолчанию
            longPollVars.wait = 25;
        }

        public bool isRunning
        {
            get { return running; }
        }

        public void setRunning(bool run)
        {
            if (run != isRunning)
            {
                running = run;
                OnRunningChanged(running);
            }
        }

        public void pause()
        {
            setRunning(false);
        }

        public void resume()
        {
            getLongPollHistory();
        }

        public int wait
        {
            get { return longPollVars.wait; }
            set { longPollVars.wait = value; }
        }

        private void requestServer()
        {
            if (isRunning)
            {
                Packet getLongPollServer = new Packet("messages.getLongPollServer");
                getLongPollServer.finished += OnRequestServerFinished;
                connection.appendQuery(getLongPollServer);
            }
        }

        private void requestUpdata()
        {
            if (!isRunning)
            {
                return;
            }

            string requestUrl = string.Format("http://{0}?act=a_check&key={1}&ts={2}&wait={3}&mode=9",
                longPollVars.server,
                longPollVars.key,
                longPollVars.ts,
                longPollVars.wait);

            HttpWebRequest httpLongPoll = (HttpWebRequest)WebRequest.Create(requestUrl);
            httpLongPoll.ContentType = "application/x-www-form-urlencoded";
            httpLongPoll.Method = "POST";
            httpLongPoll.BeginGetResponse(new AsyncCallback(responseLongPoll), httpLongPoll);
        }

        private void getLongPollHistory()
        {
            Packet packet = new Packet("execute");
            string sсript = "var l=API.messages.getLongPollHistory({\"ts\":" + longPollVars.ts + "});"
                          + "var p=API.getProfiles({\"uids\":l.messages@.uid+l.messages@.chat_active,\"fields\":\"photo_medium_rec,online,last_seen,sex\"});"
                          + "return {\"longPoll\":l,\"profiles\":p};";
            packet.addParam("code", sсript);
            packet.finished += getLongPollHistoryFinished;
            packet.error += getLongPollHistoryError;
            Client.Instance.connection.appendQuery(packet);
        }

        private void OnRunningChanged(bool running)
        {
            if (running)
            {
                requestServer();
            }
        }

        private void OnRequestServerFinished(object sender, PacketEventArgs e)
        {
            try
            {
                JObject result = JObject.Parse(e.result);
                JToken response = result["response"];
                longPollVars.server = response["server"].ToString();
                longPollVars.key = response["key"].ToString();
                longPollVars.ts = response["ts"].ToString();
                requestUpdata();
            }
            catch
            {
                //todo вызвать повторно requestServer()
            }
        }

        private void responseLongPoll(IAsyncResult e)
        {
            if (!isRunning)
            {
                return;
            }

            string response = null;
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)e.AsyncState;
                HttpWebResponse webResponse = (HttpWebResponse)webRequest.EndGetResponse(e);
                StreamReader responseReader = new StreamReader(webResponse.GetResponseStream());
                response = responseReader.ReadToEnd();
            }
            catch (WebException webException)
            {
                //todo вызвать повторно requestServer()
                return;
            }

            //проверяем на ошибки
            if (string.IsNullOrEmpty(response) || response.IndexOf("failed") >= 0)
            {
                requestServer();
                return;
            }

            try
            {
                Debug.WriteLine(response);//todo убрать
                JObject json = JObject.Parse(response);
                longPollVars.ts = json["ts"].ToString();
                JArray updates = (JArray)json["updates"];
                handler(updates);
            }
            catch
            {
            }

            requestUpdata();
        }

        void getLongPollHistoryFinished(object sender, PacketEventArgs e)
        {
            Debug.WriteLine(e.result);//todo убрать

            JObject json = JObject.Parse(e.result);
            JToken response = json["response"];

            try
            {
                EventHandler<MessageEventArgs> handlerMessageAdded = messageAdded;
                if (handlerMessageAdded != null)
                {
                    Dictionary<int, ProfileItem> profiles = new Dictionary<int, ProfileItem>();

                    foreach (var item in (JArray)response["profiles"])
                    {
                        ProfileItem profile = ProfileParser.parser(item);
                        profiles[profile.uid] = profile;
                    }

                    JArray messagesArray = (JArray)response["longPoll"]["messages"];
                    for (int i = 1; i < messagesArray.Count; i++)
                    {
                        JToken messageToken = messagesArray[i];

                        if (messageToken["deleted"] != null)
                        {
                            continue;
                        }

                        MessageItem message = MessageParser.parser(messageToken);
                        message.firstName = profiles[message.uid].firstName;
                        message.lastName = profiles[message.uid].lastName;
                        message.sex = profiles[message.uid].sex;

                        if (message.isGroupChat)
                        {
                            message.displayName = message.title;

                            try
                            {
                                string[] chatUsers = message.chatActive.Split(',');
                                message.photoMediumRec = profiles[int.Parse(chatUsers[0])].photoMediumRec;
                                message.photoMediumRec2 = profiles[int.Parse(chatUsers[1])].photoMediumRec;
                                message.photoMediumRec3 = profiles[int.Parse(chatUsers[2])].photoMediumRec;
                                message.photoMediumRec4 = profiles[int.Parse(chatUsers[3])].photoMediumRec;
                            }
                            catch
                            {
                            }

                            message.online = false;
                        }
                        else
                        {
                            message.displayName = profiles[message.uid].fullName;
                            message.photoMediumRec = profiles[message.uid].photoMediumRec;
                            message.online = profiles[message.uid].online;
                        }

                        message.lastSeen = profiles[message.uid].lastSeen;

                        MessageEventArgs arg = new MessageEventArgs();
                        arg.message = message;
                        handlerMessageAdded(this, arg);
                    }
                }
            }
            catch
            {
            }

            try
            {
                JArray updates = (JArray)response["longPoll"]["history"];
                handler(updates);
            }
            catch
            {
            }

            setRunning(true);
        }

        void getLongPollHistoryError(object sender, ErrorEventArgs e)
        {
            if (!e.fatal)
            {
                //todo обновить все в фоне
                setRunning(true);
            }
        }

        private void handler(JArray updates)
        {
            foreach (var update in updates)
            {
                try
                {
                    UpdateType updateType = (UpdateType)((int)update[0]);
                    switch (updateType)
                    {
                        case UpdateType.MessageDeleted:
                            {
                                OnMessageDeleted(update);
                                break;
                            }

                        case UpdateType.MessageFlagsReplaced:
                            {
                                OnMessageFlagsReplaced(update);
                                break;
                            }

                        case UpdateType.MessageFlagsSet:
                            {
                                OnMessageFlagsSet(update);
                                break;
                            }

                        case UpdateType.MessageFlagsReseted:
                            {
                                OnMessageFlagsReseted(update);
                                break;
                            }

                        case UpdateType.MessageAdded:
                            {
                                OnMessageAdded(update);
                                break;
                            }

                        case UpdateType.UserOnline:
                            {
                                OnUserOnline(update);
                                break;
                            }

                        case UpdateType.UserOffline:
                            {
                                OnUserOffline(update);
                                break;
                            }

                        case UpdateType.GroupChatUpdated:
                            {
                                OnGroupChatUpdated(update);
                                break;
                            }

                        case UpdateType.ChatTyping:
                            {
                                OnChatTyping(update);
                                break;
                            }

                        case UpdateType.GroupChatTyping:
                            {
                                OnGroupChatTyping(update);
                                break;
                            }

                        case UpdateType.UserCall:
                            {
                                OnUserCall(update);
                                break;
                            }
                    }
                }
                catch
                {
                }
            }
        }

        protected void OnMessageDeleted(JToken update)
        {
            if (messageDeleted != null)
            {
                MessageDeletedEventArgs arg = new MessageDeletedEventArgs();
                arg.mid = (int)update[1];
                messageDeleted(this, arg);
            }
        }

        protected void OnMessageFlagsReplaced(JToken update)
        {
            if (messageFlagsReplaced != null)
            {
                MessageFlagsReplacedEventArgs arg = new MessageFlagsReplacedEventArgs();
                arg.mid = (int)update[1];
                arg.flags = (int)update[2];
                messageFlagsReplaced(this, arg);
            }
        }

        protected void OnMessageFlagsSet(JToken update)
        {
            if (messageFlagsSet != null)
            {
                MessageFlagsSetEventArgs arg = new MessageFlagsSetEventArgs();
                arg.mid = (int)update[1];
                arg.mask = (int)update[2];
                arg.uid = (int)update[3];
                messageFlagsSet(this, arg);
            }
        }

        protected void OnMessageFlagsReseted(JToken update)
        {
            if (messageFlagsReseted != null)
            {
                MessageFlagsResetedEventArgs arg = new MessageFlagsResetedEventArgs();
                arg.mid = (int)update[1];
                arg.mask = (int)update[2];
                arg.uid = (int)update[3];
                messageFlagsReseted(this, arg);
            }
        }

        protected void OnMessageAdded(JToken update)
        {
            if (messageAdded != null)
            {
                Dictionary<int, ProfileItem> profiles = new Dictionary<int, ProfileItem>();

                foreach (var item in (JArray)update[1]["profiles"])
                {
                    ProfileItem profile = ProfileParser.parser(item);
                    profiles[profile.uid] = profile;
                }

                MessageItem message = MessageParser.parser(update[1]["message"]);
                message.firstName = profiles[message.uid].firstName;
                message.lastName = profiles[message.uid].lastName;
                message.displayName = message.isGroupChat ? message.title : profiles[message.uid].fullName;
                message.sex = profiles[message.uid].sex;
                message.photoMediumRec = profiles[message.uid].photoMediumRec;
                message.lastSeen = profiles[message.uid].lastSeen;
                if (message.lastSeen != 0 && !message.isGroupChat)
                {
                    message.online = true;//todo вычислить через lastSeen?
                }
                else
                {
                    message.online = false;
                }

                MessageEventArgs arg = new MessageEventArgs();
                arg.message = message;
                messageAdded(this, arg);
            }
        }

        protected void OnUserOnline(JToken update)
        {
            if (userStatusChanged != null)
            {
                UserStatusChangedEventArgs arg = new UserStatusChangedEventArgs();
                arg.uid = Math.Abs((int)update[1]);
                arg.online = true;
                userStatusChanged(this, arg);
            }
        }

        protected void OnUserOffline(JToken update)
        {
            if (userStatusChanged != null)
            {
                UserStatusChangedEventArgs arg = new UserStatusChangedEventArgs();
                arg.uid = Math.Abs((int)update[1]);
                arg.online = false;
                userStatusChanged(this, arg);
            }
        }

        protected void OnGroupChatUpdated(JToken update)
        {
            if (groupChatUpdated != null)
            {
                GroupChatUpdatedEventArgs arg = new GroupChatUpdatedEventArgs();
                arg.chat_id = (int)update[1];
                arg.self = ((int)update[2]) == 1;
                groupChatUpdated(this, arg);
            }
        }

        protected void OnChatTyping(JToken update)
        {
            if (chatTyping != null)
            {
                ChatTypingEventArgs arg = new ChatTypingEventArgs();
                arg.uid = (int)update[1];
                arg.flags = (int)update[2];
                chatTyping(this, arg);
            }
        }

        protected void OnGroupChatTyping(JToken update)
        {
            if (groupChatTyping != null)
            {
                GroupChatTypingEventArgs arg = new GroupChatTypingEventArgs();
                arg.uid = (int)update[1];
                arg.chat_id = (int)update[2];
                groupChatTyping(this, arg);
            }
        }

        protected void OnUserCall(JToken update)
        {
            if (userCall != null)
            {
                UserCallEventArgs arg = new UserCallEventArgs();
                arg.uid = (int)update[1];
                arg.call_id = update[2].ToString();
                userCall(this, arg);
            }
        }
    }
}
