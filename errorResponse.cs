﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Newtonsoft.Json.Linq;

namespace TitanIM.vk
{
    public class ErrorResponse
    {
        public Error code { get; protected set; }
        public string msg { get; protected set; }
        public bool global { get; protected set; }
        public bool fatal { get; protected set; }
        public string captchaSid { get; protected set; }
        public string captchaImg { get; protected set; }

        public ErrorResponse(JObject response)
        {
            try
            {
                //ответ с ошибкой при авторизации отличается от ошибок при работе с методами
                //в авторизации "error" - текст ошибки. в методах "error" - объект
                JToken error = (JToken)response["error"];
                if (error.Type == JTokenType.String)
                {
                    if (error.ToString().ToLower().Contains("captcha"))
                    {
                        code = Error.captchaNeeded;
                    }
                    else
                    {
                        code = Error.unknownErrorOccured;
                    }

                    msg = error.ToString();
                    error = response;
                }
                else
                {
                    code = (Error)((int)error["error_code"]);
                    msg = error["error_msg"].ToString();
                }

                //капча
                if (code == Error.captchaNeeded)
                {
                    captchaSid = error["captcha_sid"].ToString();
                    captchaImg = error["captcha_img"].ToString();
                }
            }
            catch
            {
                code = Error.unknownErrorOccured;
                msg = "Unknown error occurred";
            }

            this.global = isGlobal(code);
            this.fatal = isFatal(code);
        }

        public ErrorResponse(Error code, string msg)
        {
            this.code = code;
            this.msg = msg;
            this.global = isGlobal(code);
            this.fatal = isFatal(this.code);
        }

        public static bool isGlobal(Error code)
        {
            if (code == Error.loadTokenFailed ||
                code == Error.timeoutLongPollServer ||
                code == Error.serverIsNotAvailable ||
                code == Error.unknownErrorOccured ||
                code == Error.applicationIsDisabled ||
                code == Error.unknownMethodPassed ||
                code == Error.incorrectSignature ||
                code == Error.userAuthorizationFailed ||
                code == Error.tooManyRequestsPerSecond ||
                code == Error.captchaNeeded)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool isFatal(Error code)
        {
            if (code == Error.loadTokenFailed ||
                code == Error.applicationIsDisabled ||
                code == Error.userAuthorizationFailed)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
