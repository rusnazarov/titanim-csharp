﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace TitanIM.vk
{
    public class AudioItem : AttachmentItem, INotifyPropertyChanged
    {
        int _aid;
        int _ownerId;
        string _artist;
        string _title;
        int _duration;
        string _url;
        bool _isPlaying;

        public event PropertyChangedEventHandler PropertyChanged;

        public int aid
        {
            get { return _aid; }
            set
            {
                _aid = value;
                OnPropertyChanged("aid");
            }
        }

        public int ownerId
        {
            get { return _ownerId; }
            set
            {
                _ownerId = value;
                OnPropertyChanged("ownerId");
            }
        }

        public string artist
        {
            get { return _artist; }
            set
            {
                _artist = value;
                OnPropertyChanged("artist");
            }
        }

        public string title
        {
            get { return _title; }
            set
            {
                _title = value;
                OnPropertyChanged("title");
            }
        }

        public int duration
        {
            get { return _duration; }
            set
            {
                _duration = value;
                OnPropertyChanged("duration");
            }
        }

        public string url
        {
            get { return _url; }
            set
            {
                _url = value;
                OnPropertyChanged("url");
            }
        }

        public bool isPlaying
        {
            get { return _isPlaying; }
            set
            {
                _isPlaying = value;
                OnPropertyChanged("isPlaying");
            }
        }

        public AudioItem()
        {
            attachmentType = AttachmentType.Audio;
        }

        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
