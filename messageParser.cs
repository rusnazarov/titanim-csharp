﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Newtonsoft.Json.Linq;
using System.Collections.ObjectModel;

namespace TitanIM.vk
{
    public class MessageParser
    {
        public static MessageItem parser(JToken json)
        {
            MessageItem message = new MessageItem();

            try
            {
                message.mid = json["mid"] != null ? (int)json["mid"] : -1;
                message.uid = int.Parse(json["uid"].ToString());
                message.date = Utils.convertFromUnixTimestamp(int.Parse(json["date"].ToString())).ToLocalTime();
                message.isUnread = (json["read_state"] != null && (int)json["read_state"] == 0) ? true : false;
                message.isOut = (json["out"] != null && (int)json["out"] == 1) ? true : false;
                message.body = Utils.decode((string)json["body"]);
                message.title = (string)json["title"];
                message.chatId = json["chat_id"] != null ? (int)json["chat_id"] : -1;
                message.chatUsersCount = json["users_count"] != null ? (int)json["users_count"] : 0;
                message.chatActive = json["chat_active"] != null ? (string)json["chat_active"] : null;

                JArray attachmentsArray = (JArray)json["attachments"];
                if (attachmentsArray != null)
                {
                    message.attachments = AttachmentsParser.parser(attachmentsArray);
                }

                JToken geo = (JToken)json["geo"];
                if (geo != null)
                {
                    if (message.attachments == null)
                    {
                        message.attachments = new AttachmentList();
                    }

                    message.attachments.Add(MapParser.parser(geo));
                }

                JArray forwardedMessagesArray = (JArray)json["fwd_messages"];
                if (forwardedMessagesArray != null)
                {
                    if (message.attachments == null)
                    {
                        message.attachments = new AttachmentList();
                    }

                    FwdMessagesItem forwardedMessages = new FwdMessagesItem();
                    for (int i = 0; i < forwardedMessagesArray.Count; i++)
                    {
                        JToken forwardedMessage = forwardedMessagesArray[i];
                        forwardedMessages.list.Add(MessageParser.parser(forwardedMessage));
                    }

                    message.attachments.Add(forwardedMessages);
                }
            }
            catch
            {
            }

            return message;
        }
    }
}
