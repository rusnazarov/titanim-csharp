﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace TitanIM.vk
{
    public struct LoginVars
    {
        public string grant_type;
        public string client_id;
        public string client_secret;
        public string username;
        public string password;
        public string scope;
        public string captcha_sid;
        public string captcha_key;
    }

    public struct SessionVars
    {
        public string access_token;
        public string secret;
        public string expires_in;
        public int user_id;
    };

    public struct UrlServers
    {
        public string authServer;
        public string apiServer;
    };

    public struct LongPollVars
    {
        public string server;
        public string key;
        public string ts;
        public int wait;
    };

    public enum Status
    {
        offline,
        online
    };

    public enum Sex
    {
        unknown,
        woman,
        man
    };

    public enum AreFriendsStatus
    {
        unknown = -1,
        notFriend = 0,
        outRequests = 1,
        inRequests = 2,
        friend = 3
    };

    public enum Error
    {
        loadTokenFailed = -3,
        timeoutLongPollServer = -2,
        serverIsNotAvailable = -1,
        unknownErrorOccured = 1,
        applicationIsDisabled = 2,
        unknownMethodPassed = 3,
        incorrectSignature = 4,    
        userAuthorizationFailed = 5,
        tooManyRequestsPerSecond = 6,
        deniedByUser = 7,
        internalServerError = 10,
        captchaNeeded = 14,
        accessDenied = 15,
        outOfLimits = 103,
        phoneUsedAnotherUser = 1004,
        tryLater = 1112
    };
}
