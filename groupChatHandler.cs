﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace TitanIM.vk
{
    public class GroupChatHandler
    {
        Connection connection;
        Dispatcher Dispatcher = Application.Current.RootVisual.Dispatcher;

        public event EventHandler titleChanged;
        public event EventHandler<ProfileEventArgs> addUser;
        public event EventHandler<ProfileEventArgs> removeUser;
        public event EventHandler loaded;

        public int id
        {
            get;
            protected set;
        }

        public string title
        {
            get;
            protected set;
        }

        public int adminId
        {
            get;
            protected set;
        }

        public FriendList users
        {
            get;
            protected set;
        }

        public bool isLoaded
        {
            get;
            protected set;
        }

        public GroupChatHandler(int chatId, Connection connection)
        {
            id = chatId;
            this.connection = connection;
            users = new FriendList();
            isLoaded = false;
        }

        public void update()
        {
            Packet packet = new Packet("execute");
            string sсript = "var c=API.messages.getChat({\"chat_id\":" + id.ToString() + "});"
                          + "var p=API.getProfiles({\"uids\":c.users,\"fields\":\"photo_medium_rec,online\"});"
                          + "return {\"chat\":c,\"profiles\":p};";
            packet.addParam("code", sсript);
            packet.finished += OnPacketFinished;
            connection.appendQuery(packet);
        }

        void OnPacketFinished(object sender, PacketEventArgs e)
        {
            try
            {
                JObject json = JObject.Parse(e.result);
                JToken response = json["response"];
                JToken chat = response["chat"];
                JArray profiles = (JArray)response["profiles"];

                adminId = (int)chat["admin_id"];

                string titleNew = (string)chat["title"];
                if (title != titleNew)
                {
                    title = titleNew;
                    OnTitleChanged();
                }

                if (profiles.Count != users.Count)
                {
                    for (int i = 0; i < users.Count; i++)
                    {
                        bool isRemoved = true;
                        for (int j = 0; j < profiles.Count; j++)
                        {
                            ProfileItem profile = ProfileParser.parser(profiles[j]);
                            if (profile.uid == users[i].uid)
                            {
                                isRemoved = false;
                            }
                        }

                        if (isRemoved)
                        {
                            ProfileItem removeUser = users[i];
                            Dispatcher.BeginInvoke(() =>
                            {
                                users.Remove(removeUser);
                                OnRemoveUser(removeUser);
                            });
                        }
                    }

                    for (int i = 0; i < profiles.Count; i++)
                    {
                        ProfileItem profile = ProfileParser.parser(profiles[i]);
                        if (users.IndexOf(profile.uid) == -1)
                        {
                            Dispatcher.BeginInvoke(() =>
                            {
                                users.Add(profile);
                                OnAddUser(profile);
                            });
                        }
                    }
                }

                isLoaded = true;
                OnLoaded();
            }
            catch
            {
            }
        }

        protected void OnTitleChanged()
        {
            if (titleChanged != null)
            {
                titleChanged(this, EventArgs.Empty);
            }
        }

        protected void OnAddUser(ProfileItem profile)
        {
            if (addUser != null)
            {
                ProfileEventArgs arg = new ProfileEventArgs();
                arg.profile = profile;
                addUser(this, arg);
            }
        }

        protected void OnRemoveUser(ProfileItem profile)
        {
            if (removeUser != null)
            {
                ProfileEventArgs arg = new ProfileEventArgs();
                arg.profile = profile;
                removeUser(this, arg);
            }
        }

        protected void OnLoaded()
        {
            if (loaded != null)
            {
                loaded(this, EventArgs.Empty);
            }
        }
    }
}
