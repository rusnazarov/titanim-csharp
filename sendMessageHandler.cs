﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Globalization;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace TitanIM.vk
{
    public class SendMessageHandler
    {
        Connection connection;
        Queue<MessageItem> messageQuery;
        bool isProcessing;
        UploadAttachments uploadAttachments;

        public event EventHandler sending;
        public event EventHandler<MessageSentEventArgs> successfullySent;

        public SendMessageHandler(Connection connection)
        {
            this.connection = connection;

            messageQuery = new Queue<MessageItem>();
            isProcessing = false;
            uploadAttachments = new UploadAttachments(this.connection);
            uploadAttachments.finished += OnUploadAttachmentsFinished;
        }

        public void send(MessageItem message)
        {
            messageQuery.Enqueue(message);

            if (!isProcessing)
            {
                execSendMessageQuery();
            }
        }

        void execSendMessageQuery()
        {
            if (messageQuery.Count == 0)
            {
                return;
            }

            isProcessing = true;
            MessageItem message = messageQuery.Peek();

            if (message.attachments != null && message.attachments.Count > 0)
            {
                uploadAttachments.setAttachments(message.attachments);
                uploadAttachments.upload();
                return;
            }

            sendMessage();
        }

        void sendMessage()
        {
            MessageItem message = messageQuery.Dequeue();

            Packet messagePacket = new Packet("messages.send");
            messagePacket.addParam("message", message.body.Replace("\r", "<br>"));

            if (message.isGroupChat)
            {
                messagePacket.addParam("chat_id", message.chatId);
            }
            else
            {
                messagePacket.addParam("uid", message.uid);
            }

            if (message.attachments != null)
            {
                string attachmentsString = "";
                for (int i = 0; i < message.attachments.Count; i++)
                {
                    switch (message.attachments[i].attachmentType)
                    {
                        case AttachmentType.Map:
                            {
                                MapItem map = message.attachments[i] as MapItem;
                                messagePacket.addParam("lat", map.latitude.ToString(CultureInfo.InvariantCulture));
                                messagePacket.addParam("long", map.longitude.ToString(CultureInfo.InvariantCulture));
                                break;
                            }

                        case AttachmentType.PhotoFile:
                            {
                                PhotoFileItem photoFile = message.attachments[i] as PhotoFileItem;
                                attachmentsString += string.Concat(photoFile.id, ",");
                                break;
                            }

                        case AttachmentType.FwdMessages:
                            {
                                FwdMessagesItem fwdMessages = message.attachments[i] as FwdMessagesItem;
                                messagePacket.addParam("forward_messages", fwdMessages.getMids());
                                break;
                            }
                    }
                }
                attachmentsString = attachmentsString.TrimEnd(',');
                messagePacket.addParam("attachment", attachmentsString);
            }

            messagePacket.id = message.mid;
            messagePacket.finished += OnSendMessageFinished;
            OnSending();
            connection.appendQuery(messagePacket);

            isProcessing = false;
            execSendMessageQuery();
        }

        void OnUploadAttachmentsFinished(object sender, EventArgs e)
        {
            sendMessage();
        }

        void OnSendMessageFinished(object sender, PacketEventArgs e)
        {
            Packet packet = sender as Packet;
            int oldMid = packet.id;

            JObject json = JObject.Parse(e.result);
            int newMid = (int)json["response"];

            OnSuccessfullySent(oldMid, newMid);
        }

        protected void OnSending()
        {
            if (sending != null)
            {
                sending(this, EventArgs.Empty);
            }
        }

        protected void OnSuccessfullySent(int oldMid, int newMid)
        {
            if (successfullySent != null)
            {
                MessageSentEventArgs arg = new MessageSentEventArgs();
                arg.oldMid = oldMid;
                arg.newMid = newMid;
                successfullySent(this, arg);
            }
        }
    }
}
