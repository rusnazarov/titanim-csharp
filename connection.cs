﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace TitanIM.vk
{
    public class Connection
    {
        LoginVars loginVars;
        SessionVars sessionVars;
        UrlServers urlServers;
        Status status;
        bool isProcessing;
        Queue<Packet> queryList;
        DispatcherTimer tooManyRequestsTimer;

        public event EventHandler<ConnectedEventArgs> connected;
        public event EventHandler disconnected;
        public event EventHandler<ErrorEventArgs> error;
        public event EventHandler<CaptchaEventArgs> captcha;

        public Connection(string client_id, string client_secret)
        {
            //переменные входа и получения сессии
            loginVars.grant_type = "password";
            loginVars.client_id = client_id;
            loginVars.client_secret = client_secret;
            loginVars.scope = "friends,photos,audio,video,docs,notes,status,wall,groups,messages,notifications,nohttps";

            //адреса взаимодействия с API
            urlServers.authServer = string.Format("https://api.vk.com/oauth/token?grant_type={0}&client_id={1}&client_secret={2}&scope={3}",
                                                    loginVars.grant_type,
                                                    loginVars.client_id,
                                                    loginVars.client_secret,
                                                    loginVars.scope);

            urlServers.apiServer = "http://api.vk.com";

            //по умолчанию не в сети
            status = Status.offline;

            //флаг выполнения запросов к серверу
            isProcessing = false;

            //очередь пакетов
            queryList = new Queue<Packet>();

            //таймер ошибки когда слишком много запросов в секунду
            tooManyRequestsTimer = new DispatcherTimer();
            tooManyRequestsTimer.Interval = TimeSpan.FromMilliseconds(500);
            tooManyRequestsTimer.Tick += OnTooManyRequestsTimerTick;
        }

        private void setStatus(Status status)
        {
            if (this.status != status)
            {
                this.status = status;
                OnStatusChanged(this.status);
            }
        }

        public bool isOnline
        {
            get { return status == Status.online; }
        }

        public bool isOffline
        {
            get { return status == Status.offline; }
        }

        public void connectToVk(string username, string password)
        {
            if (!isOffline || string.IsNullOrEmpty(username))
            {
                return;
            }

            //заполняем логин/пароль
            loginVars.username = username;
            loginVars.password = password;

            //получаем токен
            getToken();
        }

        public void connectToVk(int uid, string token, string secret)
        {
            if (!isOffline || string.IsNullOrEmpty(token))
            {
                return;
            }

            //заполняем
            sessionVars.user_id = uid;
            sessionVars.access_token = token;
            sessionVars.secret = secret;
            setStatus(Status.online);
        }

        public void disconnectVk()
        {
            setStatus(Status.offline);
        }

        private void getToken()
        {
            //получаем токен с помощью прямой авторизации
            string authUrl = urlServers.authServer + "&username={0}&password={1}";
            authUrl = string.Format(authUrl, loginVars.username, loginVars.password);
            //captcha
            if (!string.IsNullOrEmpty(loginVars.captcha_sid))
            {
                authUrl += "&captcha_sid={0}&captcha_key={1}";
                authUrl = string.Format(authUrl, loginVars.captcha_sid, loginVars.captcha_key);
                loginVars.captcha_sid = "";
                loginVars.captcha_key = "";
            }

            HttpWebRequest httpAuth = (HttpWebRequest)WebRequest.Create(authUrl);
            //httpAuth.AllowReadStreamBuffering = true;
            httpAuth.ContentType = "application/x-www-form-urlencoded";
            httpAuth.Method = "POST";
            httpAuth.BeginGetResponse(new AsyncCallback(getTokenFinished), httpAuth);
        }

        private void getTokenFinished(IAsyncResult e)
        {
            //получили ответ от сервера авторизации
            string response = null;
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)e.AsyncState;
                HttpWebResponse webResponse = (HttpWebResponse)webRequest.EndGetResponse(e);
                StreamReader responseReader = new StreamReader(webResponse.GetResponseStream());
                response = responseReader.ReadToEnd();
            }
            catch (WebException webException)
            {
                if (((HttpWebResponse)webException.Response).StatusCode == HttpStatusCode.Unauthorized)
                {
                    string result = new StreamReader(webException.Response.GetResponseStream()).ReadToEnd();
                    JObject resultJson = JObject.Parse(result);
                    ErrorResponse errorResponse = new ErrorResponse(resultJson);
                    if (errorResponse.code == Error.captchaNeeded)
                    {
                        OnError(errorResponse);
                    }
                    else
                    {
                        OnError(Error.userAuthorizationFailed, "User authorization failed");
                    }
                }
                else
                {
                    OnError(Error.serverIsNotAvailable, "Server is not available");
                }

                return;
            }

            JObject json = JObject.Parse(response);
            try
            {
                JToken vkToken = (JToken)json["access_token"];
                loginSuccess(json);
            }
            catch
            {
                loginFailure(json);
            }
        }

        private void loginSuccess(JObject response)
        {
            //авторизация удалась
            sessionVars.access_token = response["access_token"].ToString();
            sessionVars.secret = response["secret"].ToString();
            sessionVars.user_id = (int)response["user_id"];
            sessionVars.expires_in = response["expires_in"].ToString();

            if (!string.IsNullOrEmpty(sessionVars.access_token))
            {
                setStatus(Status.online);
            }
            else
            {
                OnError(Error.loadTokenFailed, "Load token failed");
            }
        }

        private void loginFailure(JObject response)
        {
            ErrorResponse errorResponse = new ErrorResponse(response);
            OnError(errorResponse);
            return;
        }

        public void appendQuery(Packet packet)
        {
            if (isOffline)
            {
                return;
            }

            //добавляется запрос в очередь
            queryList.Enqueue(packet);

            //активируем выполнения запросов
            if (!isProcessing)
            {
                execQuery();
            }
        }

        private void execQuery()
        {
            if (isOffline || queryList.Count == 0 || isProcessing)
            {
                return;
            }

            //флаг выполнения запросов
            isProcessing = true;

            //отправляем на сервер следующий запрос из очереди
            Uri requestUrl = new Uri(string.Concat(urlServers.apiServer, queryList.Peek().genQuery(sessionVars.access_token, sessionVars.secret)));
            HttpWebRequest http = (HttpWebRequest)WebRequest.Create(requestUrl);
            http.ContentType = "application/x-www-form-urlencoded";
            http.Method = "POST";
            http.BeginGetResponse(new AsyncCallback(apiResponse), http);
        }

        private void apiResponse(IAsyncResult e)
        {
            if (isOffline)
            {
                return;
            }

            string response = null;
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)e.AsyncState;
                HttpWebResponse webResponse = (HttpWebResponse)webRequest.EndGetResponse(e);
                StreamReader responseReader = new StreamReader(webResponse.GetResponseStream());
                response = responseReader.ReadToEnd();
            }
            catch (WebException webException)
            {
                if (webException.Status == WebExceptionStatus.RequestCanceled)
                {
                    isProcessing = false;
                    execQuery();
                    return;
                }

                if (((HttpWebResponse)webException.Response).StatusCode == HttpStatusCode.GatewayTimeout) //todo GatewayTimeout?
                {
                    OnError(Error.serverIsNotAvailable, "Server is not available");
                }
                else
                {
                    isProcessing = false;
                    execQuery();
                }
                return;
            }

            //парсим
            JObject json = JObject.Parse(response);
            JToken JResponse;

            //проверяем на ошибки
            if (json.TryGetValue("error", out JResponse))
            {
                ErrorResponse errorResponse = new ErrorResponse(json);

                if (errorResponse.global)
                {
                    //отправляем в обработчик глобальных ошибок
                    //цикл выполнения пакетов остановлен
                    OnError(errorResponse);
                    return;
                }
                else
                {
                    //ошибка локальная - заполняем пакет ошибкой и забываем про него
                    queryList.Dequeue().setError(errorResponse);
                }
            }
            else
            {
                //заполняем пакет ответом от сервера ВК
                queryList.Dequeue().setResult(response);
            }

            isProcessing = false;

            //если в очереди запросов еще есть что выполнять, то
            //вызываем функцию выполнения запросов
            if (queryList.Count > 0)
            {
                execQuery();
            }
        }

        public void setCaptcha(string sid, string text)
        {
            if (queryList.Count > 0)
            {
                //запрос капчи был при выполнении пакета
                queryList.Peek().addParam("captcha_sid", sid);
                queryList.Peek().addParam("captcha_key", text);
                isProcessing = false;
                execQuery();
            }
            else if (isOffline && !string.IsNullOrEmpty(text))
            {
                //запрос капчи был при авторизации
                loginVars.captcha_sid = sid;
                loginVars.captcha_key = text;
                getToken();
            }
            else
            {
                OnError(Error.userAuthorizationFailed, "User authorization failed");
            }
        }

        void OnTooManyRequestsTimerTick(object sender, EventArgs e)
        {
            tooManyRequestsTimer.Stop();
            isProcessing = false;
            if (queryList.Count > 0)
            {
                execQuery();
            }
        }

        protected void OnStatusChanged(Status status)
        {
            switch (status)
            {
                case Status.online:
                    {
                        OnConnected();
                        break;
                    }

                case Status.offline:
                    {
                        //tooManyRequestsTimer.Stop();//в память о баге отправленном на конкурс :(
                        queryList.Clear();
                        isProcessing = false;
                        OnDisconnected();
                        break;
                    }
            }
        }

        protected void OnConnected()
        {
            if (connected != null)
            {
                ConnectedEventArgs arg = new ConnectedEventArgs();
                arg.token = sessionVars.access_token;
                arg.secret = sessionVars.secret;
                arg.uid = sessionVars.user_id;
                connected(this, arg);
            }
        }

        protected void OnDisconnected()
        {
            if (disconnected != null)
            {
                disconnected(this, EventArgs.Empty);
            }
        }

        protected void OnError(ErrorResponse errorResponse)
        {
            //если ошибка фатальная - дисконнект
            if (errorResponse.fatal)
            {
                setStatus(Status.offline);
            }

            //обработчик глобальных ошибок
            switch (errorResponse.code)
            {
                case Error.captchaNeeded:
                    {
                        if (captcha != null)
                        {
                            CaptchaEventArgs arg = new CaptchaEventArgs();
                            arg.captcha_sid = errorResponse.captchaSid;
                            arg.captcha_img = errorResponse.captchaImg;
                            captcha(this, arg);
                        }
                        break;
                    }

                case Error.tooManyRequestsPerSecond:
                    {
                        tooManyRequestsTimer.Start();
                        break;
                    }

                default:
                    {
                        if (error != null)
                        {
                            ErrorEventArgs arg = new ErrorEventArgs();
                            arg.error = errorResponse.code;
                            arg.text = errorResponse.msg;
                            arg.global = errorResponse.global;
                            arg.fatal = errorResponse.fatal;
                            error(this, arg);
                        }

                        if (!errorResponse.fatal)
                        {
                            //возобновляем цикл выполнения пакетов
                            queryList.Dequeue().setError(errorResponse);
                            isProcessing = false;
                            if (queryList.Count > 0)
                            {
                                execQuery();
                            }
                        }
                        break;
                    }
            }
        }

        protected void OnError(Error code, string msg)
        {
            ErrorResponse errorResponse = new ErrorResponse(code, msg);
            OnError(errorResponse);
        }
    }
}
