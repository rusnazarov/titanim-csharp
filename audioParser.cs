﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Newtonsoft.Json.Linq;

namespace TitanIM.vk
{
    public class AudioParser
    {
        public static AudioItem parser(JToken json)
        {
            AudioItem audio = new AudioItem();

            try
            {
                audio.aid = (int)json["aid"];
                audio.ownerId = (int)json["owner_id"];
                audio.artist = (string)json["artist"];
                audio.title = (string)json["title"];
                audio.duration = (int)json["duration"];
                audio.url = (string)json["url"];
            }
            catch
            {
            }

            return audio;
        }
    }
}
