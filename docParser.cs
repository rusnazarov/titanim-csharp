﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Newtonsoft.Json.Linq;

namespace TitanIM.vk
{
    public class DocParser
    {
        public static DocItem parser(JToken json)
        {
            DocItem doc = new DocItem();

            try
            {
                doc.did = (int)json["did"];
                doc.ownerId = (int)json["owner_id"];
                doc.title = (string)json["title"];
                doc.size = (int)json["size"];
                doc.ext = (string)json["ext"];
                doc.url = (string)json["url"];
                doc.thumb = json["thumb"] != null ? (string)json["thumb"] : null;
            }
            catch
            {
            }

            return doc;
        }
    }
}
