﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace TitanIM.vk
{
    public class DocItem : AttachmentItem, INotifyPropertyChanged
    {
        int _did;
        int _ownerId;
        string _title;
        int _size;
        string _ext;
        string _url;
        string _thumb;

        public event PropertyChangedEventHandler PropertyChanged;

        public int did
        {
            get { return _did; }
            set
            {
                _did = value;
                OnPropertyChanged("did");
            }
        }

        public int ownerId
        {
            get { return _ownerId; }
            set
            {
                _ownerId = value;
                OnPropertyChanged("ownerId");
            }
        }

        public string title
        {
            get { return _title; }
            set
            {
                _title = value;
                OnPropertyChanged("title");
            }
        }

        public int size
        {
            get { return _size; }
            set
            {
                _size = value;
                OnPropertyChanged("size");
            }
        }

        public string ext
        {
            get { return _ext; }
            set
            {
                _ext = value;
                OnPropertyChanged("ext");
            }
        }

        public string url
        {
            get { return _url; }
            set
            {
                _url = value;
                OnPropertyChanged("url");
            }
        }

        public string thumb
        {
            get { return _thumb; }
            set
            {
                _thumb = value;
                OnPropertyChanged("thumb");
                OnPropertyChanged("isThumb");
            }
        }

        public bool isThumb
        {
            get { return !string.IsNullOrEmpty(thumb); }
        }

        public DocItem()
        {
            attachmentType = AttachmentType.Doc;
        }

        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
