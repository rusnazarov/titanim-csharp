﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace TitanIM.vk
{
    public class FwdMessagesItem : AttachmentItem, INotifyPropertyChanged
    {
        MessageList _list;

        public event PropertyChangedEventHandler PropertyChanged;

        public MessageList list
        {
            get { return _list; }
            set
            {
                _list = value;
                OnPropertyChanged("list");
            }
        }

        public FwdMessagesItem()
        {
            _list = new MessageList();
            attachmentType = AttachmentType.FwdMessages;
        }

        public string getMids()
        {
            string mids = "";

            for (int i = 0; i < list.Count; i++)
            {
                mids += string.Concat(list[i].mid, ",");
            }

            return mids.TrimEnd(',');
        }

        public bool isMultiLevel()
        {
            bool multiLevel = false;

            for (int i = 0; i < _list.Count; i++)
            {
                if (_list[i].attachments != null && _list[i].attachments.CountItemsOfType(AttachmentType.FwdMessages) > 0)
                {
                    multiLevel = true;
                    break;
                }
            }

            return multiLevel;
        }

        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
