﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Newtonsoft.Json.Linq;

namespace TitanIM.vk
{
    public class PhotoParser
    {
        public static PhotoItem parser(JToken json)
        {
            PhotoItem photo = new PhotoItem();

            try
            {
                photo.pid = (int)json["pid"];
                photo.aid = (int)json["aid"];
                photo.ownerId = (int)json["owner_id"];

                if (json["sizes"] != null)
                {
                    photo.srcBig = (string)json["sizes"][2]["src"];
                    json = json["sizes"][2];
                }
                else
                {
                    photo.srcBig = (string)json["src_big"];
                }

                if (json["width"] != null)
                {
                    photo.width = (int)json["width"];
                    photo.height = (int)json["height"];
                }
            }
            catch
            {
            }

            return photo;
        }
    }
}
