﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Newtonsoft.Json.Linq;
using System.Collections.ObjectModel;

namespace TitanIM.vk
{
    public class AttachmentsParser
    {
        public static AttachmentList parser(JArray attachmentsArray)
        {
            AttachmentList attachments = new AttachmentList();

            try
            {
                for (int i = 0; i < attachmentsArray.Count; i++)
                {
                    string typeAttachment = (string)attachmentsArray[i]["type"];
                    JToken attachment = attachmentsArray[i][typeAttachment];
                    switch (typeAttachment)
                    {
                        case "photo":
                            {
                                PhotoItem photo = PhotoParser.parser(attachment);
                                attachments.Add(photo);
                                break;
                            }

                        case "video":
                            {
                                VideoItem video = VideoParser.parser(attachment);
                                attachments.Add(video);
                                break;
                            }

                        case "audio":
                            {
                                AudioItem audio = AudioParser.parser(attachment);
                                attachments.Add(audio);
                                break;
                            }

                        case "doc":
                            {
                                DocItem doc = DocParser.parser(attachment);
                                attachments.Add(doc);
                                break;
                            }
                    }
                }
            }
            catch
            {
            }

            return attachments;
        }
    }
}
