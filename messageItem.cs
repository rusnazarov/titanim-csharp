﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace TitanIM.vk
{
    public class MessageItem : INotifyPropertyChanged
    {
        int _mid;
        int _uid;
        DateTime _date;
        bool _isUnread;
        bool _isOut;
        bool _isError;
        bool _deliveryReport;
        string _title;
        string _body;
        int _chatId;
        string _chatActive;
        int _chatUsersCount;
        string _firstName;
        string _lastName;
        Sex _sex;
        string _photoMediumRec;
        string _photoMediumRec2;
        string _photoMediumRec3;
        string _photoMediumRec4;
        bool _online;
        int _lastSeen;
        bool _isChecked;
        AttachmentList _attachments;

        public event PropertyChangedEventHandler PropertyChanged;

        public int mid
        {
            get { return _mid; }
            set
            {
                _mid = value;
                OnPropertyChanged("mid");
                OnPropertyChanged("isSending");
            }
        }

        public int uid
        {
            get { return _uid; }
            set
            {
                _uid = value;
                OnPropertyChanged("uid");
            }
        }

        public DateTime date
        {
            get { return _date; }
            set
            {
                _date = value;
                OnPropertyChanged("date");
            }
        }

        public bool isUnread
        {
            get { return _isUnread; }
            set
            {
                _isUnread = value;
                OnPropertyChanged("isUnread");
            }
        }

        public bool isOut
        {
            get { return _isOut; }
            set
            {
                _isOut = value;
                OnPropertyChanged("isOut");
            }
        }

        public bool isSending
        {
            get { return mid < 0; }
        }

        public bool isError
        {
            get { return _isError; }
            set
            {
                _isError = value;
                OnPropertyChanged("isError");
            }
        }

        public bool deliveryReport
        {
            get { return _deliveryReport; }
            set
            {
                _deliveryReport = value;
                OnPropertyChanged("deliveryReport");
            }
        }

        public string title
        {
            get { return _title; }
            set
            {
                _title = value;
                OnPropertyChanged("title");
                OnPropertyChanged("displayName");
            }
        }

        public string body
        {
            get { return _body; }
            set
            {
                _body = value;
                OnPropertyChanged("body");
            }
        }

        public int chatId
        {
            get { return _chatId; }
            set
            {
                _chatId = value;
                OnPropertyChanged("chatId");
            }
        }

        public string chatActive
        {
            get { return _chatActive; }
            set
            {
                _chatActive = value;
                OnPropertyChanged("chatActive");
            }
        }

        public bool isGroupChat
        {
            get { return chatId == -1 ? false : true; }
        }

        public int chatUsersCount
        {
            get { return _chatUsersCount; }
            set
            {
                _chatUsersCount = value;
                OnPropertyChanged("chatUsersCount");
            }
        }

        public string firstName
        {
            get { return _firstName; }
            set
            {
                _firstName = value;
                OnPropertyChanged("firstName");
                OnPropertyChanged("displayName");
            }
        }

        public string lastName
        {
            get { return _lastName; }
            set
            {
                _lastName = value;
                OnPropertyChanged("lastName");
                OnPropertyChanged("displayName");
            }
        }

        public string displayName
        {
            get;
            set;
        }

        public Sex sex
        {
            get { return _sex; }
            set
            {
                _sex = value;
                OnPropertyChanged("sex");
            }
        }

        public string photoMediumRec
        {
            get { return _photoMediumRec; }
            set
            {
                _photoMediumRec = value;
                OnPropertyChanged("photoMediumRec");
            }
        }

        public string photoMediumRec2
        {
            get { return _photoMediumRec2; }
            set
            {
                _photoMediumRec2 = value;
                OnPropertyChanged("photoMediumRec2");
            }
        }

        public string photoMediumRec3
        {
            get { return _photoMediumRec3; }
            set
            {
                _photoMediumRec3 = value;
                OnPropertyChanged("photoMediumRec3");
            }
        }

        public string photoMediumRec4
        {
            get { return _photoMediumRec4; }
            set
            {
                _photoMediumRec4 = value;
                OnPropertyChanged("photoMediumRec4");
            }
        }

        public bool online
        {
            get { return _online; }
            set
            {
                _online = value;
                OnPropertyChanged("online");
            }
        }

        public int lastSeen
        {
            get { return _lastSeen; }
            set
            {
                _lastSeen = value;
                OnPropertyChanged("lastSeen");
            }
        }

        public bool isChecked
        {
            get { return _isChecked; }
            set
            {
                _isChecked = value;
                OnPropertyChanged("isChecked");
            }
        }

        public AttachmentList attachments
        {
            get { return _attachments; }
            set
            {
                _attachments = value;
                OnPropertyChanged("attachments");
            }
        }

        public MessageItem()
        {
            _chatId = -1;
        }

        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
