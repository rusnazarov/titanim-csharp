﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace TitanIM.vk
{
    public class VideoItem : AttachmentItem, INotifyPropertyChanged
    {
        int _vid;
        int _ownerId;
        string _title;
        int _duration;
        string _description;
        string _imageBig;
        string _url;

        public event PropertyChangedEventHandler PropertyChanged;

        public int vid
        {
            get { return _vid; }
            set
            {
                _vid = value;
                OnPropertyChanged("vid");
            }
        }

        public int ownerId
        {
            get { return _ownerId; }
            set
            {
                _ownerId = value;
                OnPropertyChanged("ownerId");
            }
        }

        public string title
        {
            get { return _title; }
            set
            {
                _title = value;
                OnPropertyChanged("title");
            }
        }

        public int duration
        {
            get { return _duration; }
            set
            {
                _duration = value;
                OnPropertyChanged("duration");
            }
        }

        public string description
        {
            get { return _description; }
            set
            {
                _description = value;
                OnPropertyChanged("description");
            }
        }

        public string imageBig
        {
            get { return _imageBig; }
            set
            {
                _imageBig = value;
                OnPropertyChanged("imageBig");
            }
        }

        public string url
        {
            get { return _url; }
            set
            {
                _url = value;
                OnPropertyChanged("url");
            }
        }

        public VideoItem()
        {
            attachmentType = AttachmentType.Video;
        }

        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
