﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Globalization;

namespace TitanIM.vk
{
    public class MapItem : AttachmentItem, INotifyPropertyChanged
    {
        double _latitude;
        double _longitude;
        int _width;
        int _height;

        public event PropertyChangedEventHandler PropertyChanged;

        public double latitude
        {
            get { return _latitude; }
            set
            {
                _latitude = value;
                OnPropertyChanged("latitude");
                OnPropertyChanged("image");
            }
        }

        public double longitude
        {
            get { return _longitude; }
            set
            {
                _longitude = value;
                OnPropertyChanged("longitude");
                OnPropertyChanged("image");
            }
        }

        public int width
        {
            get { return _width; }
            set
            {
                _width = value;
                OnPropertyChanged("width");
                OnPropertyChanged("image");
            }
        }

        public int height
        {
            get { return _height; }
            set
            {
                _height = value;
                OnPropertyChanged("height");
                OnPropertyChanged("image");
            }
        }

        public string image
        {
            get {
                return string.Format("http://maps.googleapis.com/maps/api/staticmap?zoom=12&size={0}x{1}&sensor=false&center={2}", width, height,
                                     latitude.ToString(CultureInfo.InvariantCulture) + "," + longitude.ToString(CultureInfo.InvariantCulture));
            }
        }

        public MapItem()
        {
            attachmentType = AttachmentType.Map;
        }

        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
