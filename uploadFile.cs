﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO;
using System.IO.IsolatedStorage;

namespace TitanIM.vk
{
    public class UploadFile
    {
        public event EventHandler<UploadResultEventArgs> finished;

        public int id
        {
            get;
            set;
        }

        public void upload(string url, string fileName, string fieldName, string contentType)
        {
            string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
            byte[] boundaryBytes = System.Text.Encoding.UTF8.GetBytes("\r\n--" + boundary + "\r\n");

            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);
            wr.ContentType = "multipart/form-data; boundary=" + boundary;
            wr.Method = "POST";
            wr.UseDefaultCredentials = true;
            wr.BeginGetRequestStream(new AsyncCallback(e =>
            {
                try
                {
                    HttpWebRequest webRequest = (HttpWebRequest)e.AsyncState;
                    Stream rs = webRequest.EndGetRequestStream(e);

                    string formItem = string.Format("Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\n", fieldName, fileName);
                    formItem += string.Format("Content-Type: {0}\r\n\r\n", contentType);
                    byte[] formItemBytes = System.Text.Encoding.UTF8.GetBytes(formItem);
                    rs.Write(boundaryBytes, 0, boundaryBytes.Length);
                    rs.Write(formItemBytes, 0, formItemBytes.Length);

                    byte[] buffer = new byte[4096];
                    int bytesRead = 0;
                    IsolatedStorageFileStream stream = IsolatedStorageFile.GetUserStoreForApplication().OpenFile(fileName, FileMode.Open);
                    while ((bytesRead = stream.Read(buffer, 0, buffer.Length)) != 0)
                    {
                        rs.Write(buffer, 0, bytesRead);
                    }
                    stream.Close();

                    byte[] trailer = System.Text.Encoding.UTF8.GetBytes("\r\n--" + boundary + "\r\n");
                    rs.Write(trailer, 0, trailer.Length);
                    rs.Close();
                }
                catch
                {
                }

                wr.BeginGetResponse(new AsyncCallback(re =>
                {
                    string response = null;
                    try
                    {
                        HttpWebRequest webRequest = (HttpWebRequest)re.AsyncState;
                        HttpWebResponse webResponse = (HttpWebResponse)webRequest.EndGetResponse(re);
                        StreamReader responseReader = new StreamReader(webResponse.GetResponseStream());
                        response = responseReader.ReadToEnd();
                        OnFinished(response);
                        webResponse.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        wr = null;
                    }
                }), wr);
            }), wr);
        }

        protected void OnFinished(string result)
        {
            if (finished != null)
            {
                UploadResultEventArgs arg = new UploadResultEventArgs();
                arg.result = result;
                finished(this, arg);
            }
        }
    }
}
