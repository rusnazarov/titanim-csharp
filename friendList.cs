﻿using System;
using System.Collections.ObjectModel;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace TitanIM.vk
{
    public class FriendList : ObservableCollection<ProfileItem>
    {
        public FriendList()
            : base()
        {
        }

        public int IndexOf(int uid)
        {
            for (int i = 0; i < this.Count; i++)
            {
                if (this[i].uid == uid)
                {
                    return i;
                }
            }

            return -1;
        }

        public void Add(FriendList profiles)
        {
            for (int i = 0; i < profiles.Count; i++)
            {
                Add(profiles[i]);
            }
        }
    }
}
