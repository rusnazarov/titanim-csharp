﻿using System;
using System.Diagnostics; //todo убрать
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace TitanIM.vk
{
    public class Client
    {
        #region Singleton
        private sealed class SingletonCreator
        {
            private static readonly Client instance = new Client();
            public static Client Instance { get { return instance; } }
        }

        public static Client Instance
        {
            get { return SingletonCreator.Instance; }
        }
        #endregion

        static string client_id = "0";
        static string client_secret = "0";

        public Connection connection { get; protected set; }
        public LongPoll longPoll { get; protected set; }

        Signup authSignup;
        public Signup signup
        {
            get
            {
                if (authSignup == null)
                {
                    authSignup = new Signup(client_id, client_secret);
                }

                return authSignup;
            }
        }

        public int uid
        {
            get;
            protected set;
        }

        public string fullName
        {
            get;
            protected set;
        }

        public string photoMediumRec
        {
            get;
            protected set;
        }

        protected Client()
        {
            connection = new Connection(client_id, client_secret);
            connection.connected += OnConnected;
            connection.disconnected += OnDisconnected;
            connection.error += OnError;

            longPoll = new LongPoll(connection);

            authSignup = null;
            uid = 0;
            fullName = "";
            photoMediumRec = "";
        }

        public void getProfileSelf()//todo перенести
        {
            Packet selfProfile = new Packet("users.get");
            selfProfile.addParam("uids", uid);
            selfProfile.addParam("fields", "photo_medium_rec");
            selfProfile.finished += (s, args) =>
            {
                ProfileItem profile = ProfileParser.parser(args.result);
                fullName = profile.fullName;
                photoMediumRec = profile.photoMediumRec;
            };
            connection.appendQuery(selfProfile);
        }

        protected void OnConnected(object sender, ConnectedEventArgs e)
        {
            uid = e.uid;

            longPoll.setRunning(true);
            Debug.WriteLine("connecting" + " " + e.token + " " + e.secret);
        }

        protected void OnDisconnected(object sender, EventArgs e)
        {
            uid = 0;
            fullName = "";
            photoMediumRec = "";

            longPoll.setRunning(false);
            Debug.WriteLine("disconnecting");
        }

        protected void OnError(object sender, ErrorEventArgs e)
        {
            Debug.WriteLine(e.text);
        }
    }
}
