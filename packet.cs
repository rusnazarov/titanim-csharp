﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace TitanIM.vk
{
    public class Packet
    {
        string methodPacket;
        Dictionary<string, string> paramsPacket;
        string resultPacket;

        public event EventHandler<PacketEventArgs> finished;
        public event EventHandler<ErrorEventArgs> error;

        public Packet(string method)
        {
            //установливает метод API для запроса
            methodPacket = method;
            
            //параметры
            paramsPacket = new Dictionary<string, string>();
        }

        public string method
        {
            //метод API
            get { return methodPacket; }
        }

        public int id
        {
            //id пакета (необязательное свойство)
            get;
            set;
        }

        public string dataUser
        {
            //доп. свойство для пользовательской инфы
            get;
            set;
        }

        public string genQuery(string token, string secret)
        {
            //генерирует строку запроса
            addParam("access_token", token);
            string query = string.Format("/method/{0}?{1}", methodPacket, Utils.join('&', paramsPacket));
            string sig = string.Format("&sig={0}", Utils.getMd5String(string.Concat(query, secret)));
            query = string.Format("/method/{0}?{1}", methodPacket, Utils.joinAndEscape('&', paramsPacket));
            return string.Concat(query, sig);
        }

        public void addParam(string key, string value)
        {
            //добавляет/заменяет параметры запроса к API
            paramsPacket[key] = value;
        }

        public void addParam(string key, int value)
        {
            //перегруженный метод. Добавляет/изменяет параметры запроса к API
            addParam(key, value.ToString());
        }

        public string this[string key]
        {
            //метод индексации по параметру
            get
            {
                if (paramsPacket.ContainsKey(key))
                    return paramsPacket[key];
                else
                    return "";
            }
            set
            {
                addParam(key, value);
            }
        }

        public string result
        {
            //результат выполнения запроса
            get { return resultPacket; }
        }

        public void setResult(string result)
        {
            //записывает результат выполнения запроса
            resultPacket = result;

            EventHandler<PacketEventArgs> handler = finished;
            if (handler != null)
            {
                PacketEventArgs arg = new PacketEventArgs();
                arg.result = resultPacket;
                handler(this, arg);
            }
        }

        public void setError(ErrorResponse errorResponse)
        {
            EventHandler<ErrorEventArgs> handler = error;
            if (handler != null)
            {
                ErrorEventArgs arg = new ErrorEventArgs();
                arg.error = errorResponse.code;
                arg.text = errorResponse.msg;
                arg.global = errorResponse.global;
                arg.fatal = errorResponse.fatal;
                handler(this, arg);
            }
        }

        public bool containsKey(string key)
        {
            //проверка наличия параметра
            return paramsPacket.ContainsKey(key);
        }
    }
}
