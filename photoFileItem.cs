﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace TitanIM.vk
{
    public class PhotoFileItem : AttachmentItem, INotifyPropertyChanged
    {
        string _id;
        string _fileName;
        string _srcBig;
        int _uploadProgress;

        public event PropertyChangedEventHandler PropertyChanged;

        public string id
        {
            get { return _id; }
            set
            {
                _id = value;
                OnPropertyChanged("id");
            }
        }

        public string fileName
        {
            get { return _fileName; }
            set
            {
                _fileName = value;
                OnPropertyChanged("fileName");
            }
        }

        public string srcBig
        {
            get { return _srcBig; }
            set
            {
                _srcBig = value;
                OnPropertyChanged("srcBig");
            }
        }

        public int uploadProgress
        {
            get { return _uploadProgress; }
            set
            {
                _uploadProgress = value;
                OnPropertyChanged("uploadProgress");
                OnPropertyChanged("isUploading");
            }
        }

        public bool isUploading
        {
            get { return (uploadProgress > 0 && uploadProgress < 100); }
        }

        public PhotoFileItem()
        {
            attachmentType = AttachmentType.PhotoFile;
            _uploadProgress = 0;
        }

        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
