﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace TitanIM.vk
{
    public class PhotoItem : AttachmentItem, INotifyPropertyChanged
    {
        int _pid;
        int _aid;
        int _ownerId;
        string _src;
        string _srcBig;
        int _width;
        int _height;

        public event PropertyChangedEventHandler PropertyChanged;

        public int pid
        {
            get { return _pid; }
            set
            {
                _pid = value;
                OnPropertyChanged("pid");
            }
        }

        public int aid
        {
            get { return _aid; }
            set
            {
                _aid = value;
                OnPropertyChanged("aid");
            }
        }

        public int ownerId
        {
            get { return _ownerId; }
            set
            {
                _ownerId = value;
                OnPropertyChanged("ownerId");
            }
        }

        public string src
        {
            get { return _src; }
            set
            {
                _src = value;
                OnPropertyChanged("src");
            }
        }

        public string srcBig
        {
            get { return _srcBig; }
            set
            {
                _srcBig = value;
                OnPropertyChanged("srcBig");
            }
        }

        public int width
        {
            get { return _width; }
            set
            {
                _width = value;
                OnPropertyChanged("width");
            }
        }

        public int height
        {
            get { return _height; }
            set
            {
                _height = value;
                OnPropertyChanged("height");
            }
        }

        public PhotoItem()
        {
            attachmentType = AttachmentType.Photo;
        }

        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
