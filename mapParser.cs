﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Newtonsoft.Json.Linq;
using System.Globalization;

namespace TitanIM.vk
{
    public class MapParser
    {
        public static MapItem parser(JToken json)
        {
            MapItem map = new MapItem();

            try
            {
                string[] coordinates = ((string)json["coordinates"]).Split(' ');
                map.latitude = double.Parse(coordinates[0], CultureInfo.InvariantCulture);
                map.longitude = double.Parse(coordinates[1], CultureInfo.InvariantCulture);
            }
            catch
            {
            }

            return map;
        }
    }
}
