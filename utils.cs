﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using JeffWilcox.Utilities.Silverlight;

namespace TitanIM.vk
{
    public class Utils
    {
        public static string getMd5String(string source)
        {
            //генерация MD5 используется JeffWilcox (www.jeff.wilcox.name/2008/03/silverlight-2-md5)
            return MD5CryptoServiceProvider.GetMd5String(source);
        }

        public static string join(char separator, Dictionary<string, string> list)
        {
            var s = new StringBuilder();
            foreach (var i in list)
            {
                s.Append(i.Key + "=" + i.Value + separator);
            }

            return s.ToString().Trim(separator);
        }

        public static string joinAndEscape(char separator, Dictionary<string, string> list)
        {
            var s = new StringBuilder();
            foreach (var i in list)
            {
                s.Append(i.Key + "=" + Uri.EscapeDataString(i.Value) + separator);
            }

            return s.ToString().Trim(separator);
        }

        public static string decode(string str)
        {
            string temp; //todo
            temp = str.Replace("<br>", "\n");
            temp = temp.Replace("&amp;", "&");
            temp = temp.Replace("&lt;", "<");
            temp = temp.Replace("&gt;", ">");
            temp = temp.Replace("&quot;", "\"");
            temp = temp.Replace("&#33;", "!");
            temp = temp.Replace("&#036;", "$");
            temp = temp.Replace("&#092;", "\\");
            temp = temp.Replace("&#39;", "'");

            return temp;
        }

        public static DateTime convertFromUnixTimestamp(int timestamp)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return origin.AddSeconds(timestamp);
        }

        public static int convertToUnixTimestamp(DateTime date)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            TimeSpan diff = date - origin;
            return (int)Math.Floor(diff.TotalSeconds);
        }

        public static string photoUrlFix(string url)
        {
            return url.EndsWith(".gif") ? "Images/camera_b.png" : url;
        }

        public static void convertUid(int uid, out int id, out bool isGroupChat)
        {
            if (uid > 2000000000)
            {
                id = uid - 2000000000;
                isGroupChat = true;
            }
            else
            {
                id = uid;
                isGroupChat = false;
            }
        }

        public static string attachmentTypeToString(AttachmentType type)
        {
            switch (type)
            {
                case AttachmentType.Photo:
                    {
                        return "Фотография";
                    }

                case AttachmentType.Video:
                    {
                        return "Видеозапись";
                    }

                case AttachmentType.Audio:
                    {
                        return "Аудиозапись";
                    }

                case AttachmentType.Doc:
                    {
                        return "Документ";
                    }

                case AttachmentType.Map:
                    {
                        return "Местоположение";
                    }

                case AttachmentType.FwdMessages:
                    {
                        return "Пересланные сообщения";
                    }

                case AttachmentType.PhotoFile:
                    {
                        return "Фотография";
                    }

                default:
                    {
                        return "Вложение";
                    }
            }
        }
    }
}
