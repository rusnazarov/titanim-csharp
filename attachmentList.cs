﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace TitanIM.vk
{
    public class AttachmentList : ObservableCollection<AttachmentItem>
    {
        public AttachmentList()
            : base()
        {
        }

        public AttachmentList FilterByType(AttachmentType type)
        {
            AttachmentList filteredAttachmentList = new AttachmentList();

            for (int i = 0; i < this.Count; i++)
            {
                if (this[i].attachmentType == type)
                {
                    filteredAttachmentList.Add(this[i]);
                }
            }

            return filteredAttachmentList;
        }

        public int CountItemsOfType(AttachmentType type)
        {
            int count = 0;

            for (int i = 0; i < this.Count; i++)
            {
                if (this[i].attachmentType == type)
                {
                    count++;
                }
            }

            return count;
        }
    }
}
